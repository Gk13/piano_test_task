package com.piano_test_task.test_task.converters;


import com.piano_test_task.test_task.stackExchangeAPI.request.RequestParam;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class RequestParamToNameValuePairConverter implements Converter<EnumMap <RequestParam, String>, List<NameValuePair>> {

    @Override
    public List<NameValuePair> convert(EnumMap<RequestParam, String> source) {
        List <NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
        for (Map.Entry <RequestParam, String> sourceParam: source.entrySet()) {
            NameValuePair nameValuePair = new BasicNameValuePair(sourceParam.getKey().name(), sourceParam.getValue());
            nameValuePairList.add(nameValuePair);
            System.out.println("name: " + sourceParam.getKey().name() + " value: " + sourceParam.getValue());
        }
        return nameValuePairList;
    }
}
