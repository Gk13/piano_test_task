package com.piano_test_task.test_task.converters;

import org.springframework.core.convert.converter.Converter;

import java.util.Date;

public class UnixTimeToDateConverter implements Converter <Long, Date> {
    @Override
    public Date convert(Long source) {
        return new Date (source*1000);
    }
}
