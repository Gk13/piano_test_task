package com.piano_test_task.test_task.controllers;

import com.piano_test_task.test_task.stackExchangeAPI.response.Question;
import com.piano_test_task.test_task.stackExchangeAPI.service.StackExchangeService;
import org.springframework.web.client.RestClientException;

import java.net.URISyntaxException;
import java.util.List;

public class SearchController {
    StackExchangeService service=new StackExchangeService();

    public List <Question> search (String intitle) throws URISyntaxException, RestClientException {
        return service.search(intitle);
    }
}
