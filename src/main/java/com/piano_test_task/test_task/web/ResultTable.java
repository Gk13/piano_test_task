package com.piano_test_task.test_task.web;


import com.piano_test_task.test_task.converters.UnixTimeToDateConverter;
import com.piano_test_task.test_task.stackExchangeAPI.response.Question;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;

import java.util.List;

public class ResultTable extends Grid<Question> {

    public ResultTable() {
        this.setThemeName("mystyle");
        this.addColumn(Question::getTitle).setHeader("Заголовок").setResizable(true);
        this.addComponentColumn(Question -> {
            Anchor anchor = new Anchor();
            anchor.setText(Question.getLink());
            anchor.setTarget("_blank");
            anchor.setHref(Question.getLink());
            return anchor;
        }).setHeader("Ссылка")
                .setFlexGrow(0)
                .setResizable(true);
        this.addColumn(Question -> {
            UnixTimeToDateConverter unixTimeToDateConverter = new UnixTimeToDateConverter();
            return unixTimeToDateConverter.convert(Question.getCreation_date());
        }).setHeader("Дата создания");
        this.addColumn(Question -> {
            return Question.getOwner().getDisplayName();
        }).setHeader("Автор").getElement();

        this.addComponentColumn(Question -> {
            Icon icon;
            if (Question.isIs_answered())
                icon = VaadinIcon.CHECK.create();
            else
                icon = VaadinIcon.CLOSE.create();
            return icon;
        }).setHeader("Есть ответ");
    }

    public void refresh(List<Question> questions) {
        this.setItems(questions);
    }


}
