package com.piano_test_task.test_task.web;

import com.piano_test_task.test_task.controllers.SearchController;
import com.piano_test_task.test_task.stackExchangeAPI.response.Question;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import javax.annotation.PostConstruct;
import java.util.List;

@Route ("main")
public class MainWindow extends VerticalLayout {

    SearchLayout layout = new SearchLayout();
    SearchButton button = new SearchButton();
    ResultTable resultTable = new ResultTable ();
    @PostConstruct
    private void show () {
        button.setHeight("100%");
        button.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> event) {
                search();
            }
        });
        layout.add(button);
        layout.setAlignItems(FlexComponent.Alignment.STRETCH);
        this.add(layout);
        resultTable.setVisible(false);
        this.add(resultTable);

        HorizontalLayout lay = new HorizontalLayout();
        this.add(lay);
    }

    public void search () {
        SearchController controller = new SearchController();
        try {
            List<Question> questions = controller.search(layout.getSearchParamValue());
            resultTable.refresh(questions);
            resultTable.setVisible(true);
        } catch ( Exception e) {
            ErrorWindow errorWindow = new ErrorWindow();
            errorWindow.open();
        }

    }
}
