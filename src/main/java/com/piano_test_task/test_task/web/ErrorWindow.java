package com.piano_test_task.test_task.web;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;


public class ErrorWindow extends Dialog {
    Label errorLabel = new Label("Возникла ошибка, попробуйте выполнить поиск позже");
    Button closeButton = new Button("Ок");
    VerticalLayout layout = new VerticalLayout();
    public ErrorWindow() {
        closeButton.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> event) {
                close();
            }
        });
        layout.add(errorLabel);
        layout.add(closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        this.add(layout);
        this.setCloseOnEsc(true);

    }
}
