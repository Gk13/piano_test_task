package com.piano_test_task.test_task.web;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;


public class SearchLayout extends HorizontalLayout {
    Label searchTitle = new Label("Введите текст запроса:");
    TextField searchParam=new TextField("");

    public SearchLayout() {
        this.add(searchTitle);
        this.add(searchParam);
        this.setWidth("100%");
    }

    public String getSearchParamValue() {
        return searchParam.getValue();
    }
}
