package com.piano_test_task.test_task.stackExchangeAPI.service;

import com.piano_test_task.test_task.stackExchangeAPI.RequestBuilder;
import com.piano_test_task.test_task.stackExchangeAPI.response.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.net.URISyntaxException;
import java.util.List;

@Service
public class StackExchangeService {
    @Autowired
    private StackExchangeClient stackExchangeClient;

    public List <Question> search (String intitle) throws URISyntaxException, RestClientException {
        RequestBuilder builder = new RequestBuilder(intitle);
        stackExchangeClient=new StackExchangeClient();
        return stackExchangeClient.search(builder.getRequest());
    }
}
