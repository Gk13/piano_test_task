package com.piano_test_task.test_task.stackExchangeAPI.service;

import com.piano_test_task.test_task.stackExchangeAPI.Request;
import com.piano_test_task.test_task.stackExchangeAPI.response.Question;

import java.util.List;

public interface SearchByTitle {
    List<Question> search ( Request request) throws Exception;
}
