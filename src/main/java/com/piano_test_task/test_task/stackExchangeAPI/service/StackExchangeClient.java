package com.piano_test_task.test_task.stackExchangeAPI.service;

import com.piano_test_task.test_task.stackExchangeAPI.Request;
import com.piano_test_task.test_task.stackExchangeAPI.request.url.GenerateUrlImpl;
import com.piano_test_task.test_task.stackExchangeAPI.response.Question;
import com.piano_test_task.test_task.stackExchangeAPI.response.QuestionResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Component
public class StackExchangeClient implements SearchByTitle{
    HttpClient httpClient = HttpClientBuilder.create().build();
    ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory( httpClient);
    private RestTemplate restTemplate = new RestTemplate(requestFactory);

    public List <Question> search (Request request) throws URISyntaxException, RestClientException {
            GenerateUrlImpl generateUrl = new GenerateUrlImpl();
            URI uri = generateUrl.generate(request);
            QuestionResponse response = restTemplate.getForObject(uri, QuestionResponse.class);
            return response.getItems();
    }
}
