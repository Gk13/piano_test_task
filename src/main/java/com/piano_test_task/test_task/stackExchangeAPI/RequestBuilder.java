package com.piano_test_task.test_task.stackExchangeAPI;

import com.piano_test_task.test_task.stackExchangeAPI.request.Order;
import com.piano_test_task.test_task.stackExchangeAPI.request.RequestParam;
import com.piano_test_task.test_task.stackExchangeAPI.request.Sort;

import java.util.EnumMap;

public class RequestBuilder {
    private Request request;
    private static final String site = "stackoverflow";

    public RequestBuilder(String intitle) {
        init();
        request.getRequestParams().put(RequestParam.intitle, intitle);
    }

    void init() {
        EnumMap<RequestParam, String> requestParams = new EnumMap<RequestParam, String>(RequestParam.class);
        requestParams.put(RequestParam.site, site);
        requestParams.put(RequestParam.order, Order.desc.name());
        requestParams.put(RequestParam.sort, Sort.activity.name());
        request = new Request(requestParams);
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
