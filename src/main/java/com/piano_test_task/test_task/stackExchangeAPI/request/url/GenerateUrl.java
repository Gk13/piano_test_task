package com.piano_test_task.test_task.stackExchangeAPI.request.url;


import com.piano_test_task.test_task.stackExchangeAPI.Request;

import java.net.URI;
import java.net.URISyntaxException;


public interface GenerateUrl {
    URI generate(Request request) throws URISyntaxException;
}
