package com.piano_test_task.test_task.stackExchangeAPI.request.url;

import com.piano_test_task.test_task.converters.RequestParamToNameValuePairConverter;
import com.piano_test_task.test_task.stackExchangeAPI.Request;
import org.apache.http.client.utils.URIBuilder;

import java.net.URI;
import java.net.URISyntaxException;

public class GenerateUrlImpl implements GenerateUrl {

    @Override
    public URI generate(Request request) throws URISyntaxException {
        RequestParamToNameValuePairConverter converter = new RequestParamToNameValuePairConverter();
        URI uri =  new URIBuilder().setScheme(UrlConstants.SCHEMA).setHost(UrlConstants.STACK_EXCHANGE_API).setPath(UrlConstants.PATH).setParameters(converter.convert(request.getRequestParams())).build();        System.out.println(uri.toString());
        return uri;
    }
}
