package com.piano_test_task.test_task.stackExchangeAPI.request.url;

public class UrlConstants {
    public final static String STACK_EXCHANGE_API = "api.stackexchange.com";
    public final static String PATH="/2.2/search";
    public final static String SCHEMA="http";
}
