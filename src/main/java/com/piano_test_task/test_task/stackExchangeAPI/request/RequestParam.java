package com.piano_test_task.test_task.stackExchangeAPI.request;

public enum RequestParam {
    order,
    sort,
    intitle,
    site,
}
