package com.piano_test_task.test_task.stackExchangeAPI.request;

public enum Sort {
    activity,
    votes,
    creation,
    relevance;
}
