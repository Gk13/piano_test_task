package com.piano_test_task.test_task.stackExchangeAPI;


import com.piano_test_task.test_task.stackExchangeAPI.request.RequestParam;

import java.util.EnumMap;

public class Request {
    EnumMap <RequestParam, String> requestParams;

    public Request(EnumMap<RequestParam, String> requestParams) {
        this.requestParams = requestParams;
    }

    public EnumMap<RequestParam, String> getRequestParams() {
        return requestParams;
    }

    public void setRequestParams(EnumMap<RequestParam, String> requestParams) {
        this.requestParams = requestParams;
    }
}
