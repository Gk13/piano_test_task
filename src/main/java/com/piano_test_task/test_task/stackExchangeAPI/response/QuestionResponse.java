package com.piano_test_task.test_task.stackExchangeAPI.response;

import java.util.List;

public class QuestionResponse {
    private List<Question> items;

    public List<Question> getItems() {
        return items;
    }

    public void setItems(List<Question> items) {
        this.items = items;
    }
}
