package com.piano_test_task.test_task.stackExchangeAPI.response;


public class Question {
    private boolean is_answered;
    private String link;
    private long creation_date;
    private String title;
    private Owner owner;

    @Override
    public String toString() {
        return "Question{" +
                "is_answered=" + is_answered +
                ", link='" + link + '\'' +
                ", creation_date=" + creation_date +
                ", title='" + title + '\'' +
                '}';
    }

    public boolean isIs_answered() {
        return is_answered;
    }

    public void setIs_answered(boolean is_answered) {
        this.is_answered = is_answered;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public long getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(long creation_date) {
        this.creation_date = creation_date;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
