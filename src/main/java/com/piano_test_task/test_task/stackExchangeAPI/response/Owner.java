package com.piano_test_task.test_task.stackExchangeAPI.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Owner {
    @JsonProperty ("display_name")
    private String display_name;

    public String getDisplayName() {
        return display_name;
    }

    public void setDisplayName(String display_name) {
        this.display_name = display_name;
    }
}
